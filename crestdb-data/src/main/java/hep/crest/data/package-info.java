/**
 * This is the main package. Application.java class needs to be at this level so
 * that all spring based beans can be found.
 *
 * @version %I%, %G%
 * @author formica
 */
package hep.crest.data;
// Main root package
