package hep.crest.server.swagger.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import hep.crest.data.config.CrestProperties;
import hep.crest.data.exceptions.AbstractCdbServiceException;
import hep.crest.data.exceptions.CdbBadRequestException;
import hep.crest.data.exceptions.CdbInternalException;
import hep.crest.data.exceptions.ConflictException;
import hep.crest.data.exceptions.PayloadEncodingException;
import hep.crest.data.handlers.PayloadHandler;
import hep.crest.server.caching.CachingPolicyService;
import hep.crest.server.repositories.PayloadDataBaseCustom;
import hep.crest.server.services.IovService;
import hep.crest.server.services.PayloadService;
import hep.crest.server.services.TagService;
import hep.crest.server.swagger.api.NotFoundException;
import hep.crest.server.swagger.api.PayloadsApiService;
import hep.crest.server.swagger.model.GenericMap;
import hep.crest.server.swagger.model.HTTPResponse;
import hep.crest.server.swagger.model.IovDto;
import hep.crest.server.swagger.model.IovSetDto;
import hep.crest.server.swagger.model.PayloadDto;
import hep.crest.server.swagger.model.PayloadSetDto;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Rest endpoint for payloads.
 *
 * @author formica
 */
@Component
@Slf4j
public class PayloadsApiServiceImpl extends PayloadsApiService {

    /**
     * Set separator.
     */
    private static final String SLASH = File.pathSeparator.equals(":") ? "/" : File.pathSeparator;
    /**
     * Maximum number of files.
     */
    private static final int MAX_FILE_UPLOAD = 1000;
    /**
     * The list of payload types for download.
     */
    private static final List<String> payloadlist = Arrays.asList("png", "svg", "json", "xml", "csv", "txt", "tgz",
            "gz", "pdf");
    /**
     * Service.
     */
    @Autowired
    private PayloadService payloadService;
    /**
     * Repository.
     */
    @Autowired
    @Qualifier("payloaddatadbrepo")
    private PayloadDataBaseCustom payloaddataRepository;
    /**
     * Service.
     */
    @Autowired
    private IovService iovService;
    /**
     * Service.
     */
    @Autowired
    private TagService tagService;
    /**
     * Service.
     */
    @Autowired
    private CachingPolicyService cachesvc;
    /**
     * Properties.
     */
    @Autowired
    private CrestProperties cprops;
    /**
     * Mapper.
     */
    @Inject
    private ObjectMapper jacksonMapper;
    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /* (non-Javadoc)
     * @see hep.crest.server.swagger.api.PayloadsApiService#createPayload(hep.crest.swagger.model.PayloadDto, javax
     * .ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response createPayload(PayloadDto body, SecurityContext securityContext, UriInfo info) {
        // Create a new payload using the body in the request.
        // Verify if hash exists
        String dbhash = payloaddataRepository.exists(body.getHash());
        if (dbhash != null && dbhash.length() > 0) {
            throw new ConflictException("Hash already exists " + body.getHash());
        }
        final PayloadDto saved = payloadService.insertPayload(body);
        log.debug("Saved PayloadDto {}", saved);
        return Response.created(info.getRequestUri()).entity(saved).build();
    }

    @Override
    public Response createPayloadMultiForm(FormDataBodyPart fileBodypart, PayloadDto payload,
                                           SecurityContext securityContext, UriInfo info) {
        this.log.info("PayloadRestController processing request to upload payload from stream");
        try {
            // Assume the FormDataBodyPart is a JSON string.
            // Get the DTO.
            log.debug("Received body json " + payload);
            // Verify if hash exists
            String dbhash = payloaddataRepository.exists(payload.getHash());
            if (dbhash != null && dbhash.length() > 0) {
                throw new ConflictException("Hash already exists " + payload.getHash());
            }
            // Create the payload taking binary content from the input stream.
            FormDataContentDisposition fileDetail = fileBodypart.getFormDataContentDisposition();
            InputStream fileInputStream = fileBodypart.getValueAs(InputStream.class);
            // Create the payload taking binary content from the input stream.
            final PayloadDto saved = payloadService.insertPayloadAndInputStream(payload,
                    fileInputStream);
            return Response.created(info.getRequestUri()).entity(saved).build();
        }
        catch (final NullPointerException e) {
            // Exception, send 500.
            final String msg = "Error creating payload resource : " + e.getCause();
            throw new CdbInternalException(msg);
        }
    }

    //     @CacheControlCdb("public, max-age=604800") : this has to be set on the API class itself.
    //     For the moment we decide to use the cachecontrol filter (if active) via the method
    //     name definition, by looking for the annotation @Path
    @Override
    public Response getPayload(String hash, String format, SecurityContext securityContext,
                               UriInfo info) {
        this.log.info(
                "PayloadRestController processing request to download payload {} using format {}",
                hash, format);
        // Get only metadata from the payload.
        final PayloadDto pdto = payloadService.getPayloadMetaInfo(hash);
        final String ptype = pdto.getObjectType();
        log.debug("Found metadata {}", pdto);
        // Get the media type. It utilize the objectType field.
        final MediaType media_type = getMediaType(ptype);

        // Set caching policy depending on snapshot argument
        // this is filling a mag-age parameter in the header
        final CacheControl cc = cachesvc.getPayloadCacheControl();

        if (format == null || format.equalsIgnoreCase("BLOB")
            || format.equalsIgnoreCase("BIN")) {
            // The client requested to get binary data.
            // Get the payload data.
            final InputStream in = payloadService.getPayloadData(hash);
            // Stream data in output.
            final StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException {
                    try {
                        int read = 0;
                        final byte[] bytes = new byte[2048];
                        // Read input bytes and write in output stream
                        while ((read = in.read(bytes)) != -1) {
                            os.write(bytes, 0, read);
                            log.trace("Copying {} bytes into the output...", read);
                        }
                        // Flush data
                        os.flush();
                    }
                    catch (final Exception e) {
                        throw new WebApplicationException(e);
                    }
                    finally {
                        // Close all streames to avoid memory leaks.
                        log.debug("closing streams...");
                        if (os != null) {
                            os.close();
                        }
                        if (in != null) {
                            in.close();
                        }
                    }
                }
            };
            log.debug("Send back the stream....");
            // Get media type
            final String rettype = media_type.toString();
            // Get extension
            final String ext = getExtension(ptype);
            final String fname = hash + "." + ext;
            // Set the content type in the response, and the file name as well.
            return Response.ok(stream) /// MediaType.APPLICATION_JSON_TYPE)
                    .header("Content-type", rettype)
                    .header("Content-Disposition", "Inline; filename=\"" + fname + "\"")
                    // .header("Content-Length", new
                    // Long(f.length()).toString())
                    .cacheControl(cc)
                    .build();
        }
        else {
            // The client requested to get a DTO.
            log.debug("Retrieve the full pojo with hash {}", hash);
            final PayloadDto entity = payloadService.getPayload(hash);
            // Build the DTO Set for the response.
            final PayloadSetDto psetdto = buildSet(entity, hash);
            return Response.ok()
                    .header("Content-type", MediaType.APPLICATION_JSON_TYPE.toString())
                    .entity(psetdto)
                    .cacheControl(cc)
                    .build();
        }
    }


    @Override
    public Response storePayloadWithIovMultiForm(FormDataBodyPart fileBodypart, String tag, BigDecimal since,
                                                 String format,
                                                 String objectType, String version, BigDecimal endtime,
                                                 String streamerInfo,
                                                 SecurityContext securityContext, UriInfo info) {
        this.log.info(
                "PayloadRestController processing request to store payload with iov in tag {} and at since {} using "
                + "format {}",
                tag, since, format);
        try {
            if (fileBodypart == null) {
                throw new CdbBadRequestException("Cannot upload payload: form is missing the body part");
            }
            FormDataContentDisposition fileDetail = fileBodypart.getFormDataContentDisposition();
            InputStream fileInputStream = fileBodypart.getValueAs(InputStream.class);
            // Store payload with multi form
            if (fileDetail == null || tag == null || since == null || fileInputStream == null) {
                throw new CdbBadRequestException("Cannot upload payload: form is missing a field");
            }
            tagService.findOne(tag); // use to send back a NotFound if the tag does not exists.

            String fdetailsname = fileDetail.getFileName();
            if (fdetailsname == null || fdetailsname.isEmpty()) {
                // Generate a fake filename.
                fdetailsname = ".blob";
            }
            else {
                // Get the filename from the input request.
                final Path p = Paths.get(fdetailsname);
                fdetailsname = "_" + p.getFileName().toString();
            }
            // Create a temporary file name from tag name and time of validity.
            String filename = cprops.getDumpdir() + SLASH + tag + "_" + since
                              + fdetailsname;
            if (format == null) {
                format = "JSON";
            }
            // Add object type
            if (objectType == null) {
                objectType = format;
            }
            // Add version
            if (version == null) {
                version = "default";
            }
            log.debug("Fill meta types: {} {} {} use file name {}", objectType, version, format, filename);
            // Create the streamer info object as a map with metadata like format and filename for the moment. In
            // future this could have further informations on payload metadata (author, checksum, etc).
            final Map<String, String> sinfomap = new HashMap<>();
            // Add the filename, depending on the input information
            sinfomap.put("filename", (fileDetail.getFileName() != null && !fileDetail.getFileName().isEmpty()) ?
                    fileDetail.getFileName() : filename);
            sinfomap.put("format", format);
            sinfomap.put("insertionDate", new Date().toString());
            if (streamerInfo != null) {
                sinfomap.put("streamerInfo", streamerInfo);
            }
            // Create the DTO, the version here is ignored. It could be added from the Form data.
            PayloadDto pdto = new PayloadDto().objectType(objectType)
                    .streamerInfo(jacksonMapper.writeValueAsBytes(sinfomap)).version(version);
            final String hash = getHash(fileInputStream, filename);
            pdto.hash(hash);
            // Verify if hash exists
            String dbhash = payloaddataRepository.exists(hash);
            if (dbhash != null && dbhash.length() > 0) {
                log.warn("Hash {} already exists, set payload dto to null to skip insertion", hash);
                pdto = null;
            }
            final IovDto iovDto = new IovDto().payloadHash(hash).since(since).tagName(tag);
            // Save iov and payload and get the response object in return.
            final HTTPResponse resp = payloadService.saveIovAndPayload(iovDto, pdto, filename);
            return Response.status(Response.Status.CREATED).entity(resp).build();
        }
        catch (IOException e) {
            log.warn("Bad request: {}", e);
            throw new CdbBadRequestException("storePayloadWithIovMultiForm IO error: " + e.getMessage());
        }
    }

    @Override
    public Response storeBatch(List<FormDataBodyPart> filesBodypart, String tag, IovSetDto iovsetupload,
                               String xCrestPayloadFormat, String objectType, String version, BigDecimal endtime,
                               String streamerInfo, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        this.log.info(
                "PayloadRestController processing request to store payload batch in tag {} with multi-iov ",
                tag);
        try {
            // Read input FormData as an IovSet object.
            if (tag == null || iovsetupload == null) {
                throw new CdbBadRequestException(
                        "Cannot upload payload in batch mode : form is missing a field, " + tag + " - " + iovsetupload);
            }
            log.info("Batch insertion of {} iovs using file formatted", iovsetupload.getSize());
            // use to send back a NotFound if the tag does not exists.
            tagService.findOne(tag);
            // Add object type.
            if (objectType == null) {
                objectType = iovsetupload.getDatatype();
            }
            // Add version.
            if (version == null) {
                version = "default";
            }
            // Set default for payload format.
            if (xCrestPayloadFormat == null && filesBodypart != null) {
                xCrestPayloadFormat = "FILE";
            }
            IovSetDto outdto = null;
            if ("FILE".equalsIgnoreCase(xCrestPayloadFormat)) {
                // Check that number of files is not too much.
                if (filesBodypart == null) {
                    throw new CdbBadRequestException("Cannot use header FILE with empty list of files");
                }
                if (filesBodypart.size() > MAX_FILE_UPLOAD) {
                    final String msg = "Too many files attached to the request...> MAX_FILE_UPLOAD = "
                                       + MAX_FILE_UPLOAD;
                    throw new CdbBadRequestException("Too many files uploaded : more than " + MAX_FILE_UPLOAD);
                }
                // Only the payload format FILE is allowed here.
                // This was created to eventually merge with other methods later on.
                outdto = storeIovs(iovsetupload, tag, objectType, version, streamerInfo, filesBodypart);
            }
            else if ("JSON".equalsIgnoreCase(xCrestPayloadFormat)) {
                outdto = storeIovs(iovsetupload, tag, objectType, version, streamerInfo, null);
            }
            else {
                throw new CdbBadRequestException("Bad header parameter: " + xCrestPayloadFormat);
            }
            return Response.created(info.getRequestUri()).entity(outdto).build();
        }
        catch (IOException e) {
            log.warn("uploadPayloadBatchWithIovMultiForm bad request: {}", e.getMessage());
            throw new CdbBadRequestException(e.getMessage());
        }
    }

    /**
     * Store iovs and payload files.
     *
     * @param dto            the IovSetDto
     * @param tag            the String
     * @param filesbodyparts the List<FormDataBodyPart>
     * @return IovSetDto
     * @throws PayloadEncodingException    If an Exception occurred
     * @throws IOException                 If an Exception occurred
     * @throws AbstractCdbServiceException if an exception occurred in insertion.
     */
    protected IovSetDto storeIovs(IovSetDto dto, String tag, String objectType, String version,
                                  String streamerInfo, List<FormDataBodyPart> filesbodyparts)
            throws IOException, AbstractCdbServiceException {
        final List<IovDto> iovlist = dto.getResources();
        final List<IovDto> savediovlist = new ArrayList<>();
        // Loop over iovs found in the Set.
        for (final IovDto piovDto : iovlist) {
            String filename = null;
            log.debug("Store from iovset the entry {}", piovDto);
            final Map<String, String> sinfomap = new HashMap<>();
            sinfomap.put("format", dto.getDatatype());
            sinfomap.put("insertionDate", new Date().toString());
            if (streamerInfo != null) {
                sinfomap.put("streamerInfo", streamerInfo);
            }
            // Here we generate objectType and version. We should probably allow for input arguments.
            PayloadDto pdto = new PayloadDto().objectType(objectType).hash("none")
                    .version(version);
            if (filesbodyparts == null) {
                log.debug("Use the hash, it represents the payload : {}", piovDto.getPayloadHash());
                // If there are no attached files, then the payloadHash contains the payload itself.
                pdto.data(piovDto.getPayloadHash().getBytes());
                final String hash = getHash(new ByteArrayInputStream(pdto.getData()), "none");
                pdto.hash(hash);
                sinfomap.put("filename", hash);
            }
            else {
                // If there are attached files, then the payload will be loaded from filename.
                log.debug("Use attached files : {}", piovDto.getPayloadHash());
                final Map<String, Object> retmap = getDocumentStream(piovDto, filesbodyparts);
                filename = (String) retmap.get("file");
                final String hash = getHash((InputStream) retmap.get("stream"), filename);
                sinfomap.put("filename", filename);
                pdto.hash(hash);
            }
            pdto.streamerInfo(jacksonMapper.writeValueAsBytes(sinfomap));
            // Verify if hash exists
            String pyldhash = pdto.getHash();

            String dbhash = payloaddataRepository.exists(pyldhash);
            if (dbhash != null && dbhash.length() > 0) {
                log.warn("Hash {} already exists, set payload dto to null to skip insertion", dbhash);
                pdto = null;
            }
            final IovDto iovDto = new IovDto().payloadHash(pyldhash).since(piovDto.getSince())
                    .tagName(tag);
            try {
                log.debug("Save IOV and Payload : {} - {} using filename {}", iovDto, pdto, filename);
                HTTPResponse resp = payloadService.saveIovAndPayload(iovDto, pdto, filename);
                log.info("PayloadService response : {}", resp);
                savediovlist.add(iovDto);
            }
            catch (final JDBCException | DataIntegrityViolationException e) {
                log.error("SQL exception when inserting {}", iovDto);
                throw new ConflictException("SQL error, cannot insert iov and payload for " + iovDto.getTagName()
                                            + ", " + iovDto.getSince()
                                            + " " + iovDto.getPayloadHash()
                                            + ": " + e.getMessage());
            }
        }
        dto.size((long) savediovlist.size());
        dto.resources(savediovlist);
        return dto;
    }

    /**
     * @param fileInputStream the InputStream
     * @param filename        the String
     * @return String. The computed hash from the byte stream.
     * @throws PayloadEncodingException If an Exception occurred
     * @throws IOException              If an Exception occurred
     */
    protected String getHash(InputStream fileInputStream, String filename)
            throws PayloadEncodingException, IOException {
        try (BufferedInputStream bis = new BufferedInputStream(fileInputStream)) {
            if (filename.equals("none")) {
                return PayloadHandler.getHashFromStream(bis);
            }
            return PayloadHandler.saveToFileGetHash(bis, filename);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.crest.server.swagger.api.PayloadsApiService#getPayloadMetaInfo(java.lang.
     * String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response getPayloadMetaInfo(String hash, SecurityContext securityContext, UriInfo info) {
        this.log.info(
                "PayloadRestController processing request for payload meta information for {}",
                hash);
        final PayloadDto entity = payloadService.getPayloadMetaInfo(hash);
        final PayloadSetDto psetdto = buildSet(entity, hash);
        return Response.ok()
                .header("Content-type", MediaType.APPLICATION_JSON_TYPE.toString())
                .entity(psetdto).build();

    }

    @Override
    public Response updatePayload(String hash, Map<String, String> body, SecurityContext securityContext,
                                  UriInfo info) {
        this.log.info(
                "PayloadRestController processing request for update payload meta information for {}",
                hash);
        // Search payload.
        PayloadDto entity = payloadService.getPayloadMetaInfo(hash);
        String sinfo = null;
        // Send a bad request if body is null.
        if (body == null) {
            throw new CdbBadRequestException("Cannot update payload with null body");
        }
        // Loop over map body keys.
        for (final String key : body.keySet()) {
            if ("streamerInfo".equals(key)) {
                // Update description.
                sinfo = body.get(key);
            }
            else {
                log.warn("Ignored key {} in updatePayload: field does not exists", key);
            }
        }
        int updated = payloadService.updatePayloadMetaInfo(hash, sinfo);
        entity = payloadService.getPayloadMetaInfo(hash);
        final PayloadSetDto psetdto = buildSet(entity, hash);
        return Response.ok()
                .header("Content-type", MediaType.APPLICATION_JSON_TYPE.toString())
                .entity(psetdto).build();
    }

    /**
     * @param mddto     the IovDto
     * @param bodyParts the List<FormDataBodyPart>
     * @return Map<String, Object>
     * @throws PayloadEncodingException If an Exception occurred
     */
    protected Map<String, Object> getDocumentStream(IovDto mddto, List<FormDataBodyPart> bodyParts)
            throws PayloadEncodingException {
        log.debug("Extracting document BLOB for file {}", mddto.getPayloadHash());
        final Map<String, Object> retmap = new HashMap<>();
        String dtofname = mddto.getPayloadHash();
        if (dtofname.startsWith("file://")) {
            dtofname = mddto.getPayloadHash().split("://")[1];
        }
        for (int i = 0; i < bodyParts.size(); i++) {
            final BodyPartEntity test = (BodyPartEntity) bodyParts.get(i).getEntity();
            final String fileName = bodyParts.get(i).getContentDisposition().getFileName();
            log.debug("Search for file {} in iovset", fileName);
            if (dtofname.contains(fileName)) {
                retmap.put("file", fileName);
                retmap.put("stream", test.getInputStream());
            }
        }
        if (retmap.isEmpty()) {
            throw new PayloadEncodingException(
                    "Cannot find file content in form data. File name = " + mddto.getPayloadHash());
        }
        return retmap;
    }

    /**
     * Utility class to better download payload data using the type.
     *
     * @param ptype the String
     * @return MediaType
     */
    protected MediaType getMediaType(String ptype) {
        MediaType media_type = MediaType.APPLICATION_OCTET_STREAM_TYPE;
        final String comp = ptype.toLowerCase();
        switch (comp) {
            case "png":
                media_type = new MediaType("image", "png");
                break;
            case "svg":
                media_type = MediaType.APPLICATION_SVG_XML_TYPE;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON_TYPE;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML_TYPE;
                break;
            case "csv":
                media_type = new MediaType("text", "csv");
                break;
            case "txt":
                media_type = MediaType.TEXT_PLAIN_TYPE;
                break;
            case "tgz":
                media_type = new MediaType("application", "x-gtar-compressed");
                break;
            case "gz":
                media_type = new MediaType("application", "gzip");
                break;
            case "pdf":
                media_type = new MediaType("application", "pdf");
                break;
            default:
                break;
        }
        return media_type;
    }

    /**
     * Set file extension in dowload.
     *
     * @param ptype the String
     * @return String
     */
    protected String getExtension(String ptype) {
        String extension = "blob";
        final String comp = ptype.toLowerCase();
        final boolean match = payloadlist.stream().anyMatch(comp::contains);
        if (match) {
            extension = comp;
        }
        return extension;
    }

    /**
     * @param entity the PayloadDto
     * @param hash   the String
     * @return PayloadSetDto
     */
    protected PayloadSetDto buildSet(PayloadDto entity, String hash) {
        final GenericMap map = new GenericMap();
        map.put("hash", hash);
        final PayloadSetDto psetdto = new PayloadSetDto().addResourcesItem(entity);
        psetdto.datatype(entity.getObjectType()).filter(map).size(1L);
        return psetdto;
    }

}
