/**
 * This file is part of Crest.
 * <p>
 * PhysCondDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Crest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Crest.  If not, see <http://www.gnu.org/licenses/>.
 **/
package hep.crest.server.repositories;

import hep.crest.data.repositories.DataGeneral;
import hep.crest.server.swagger.model.TagSummaryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

/**
 * An implementation for groups queries.
 *
 * @author formica
 *
 */
public class IovGroupsImpl extends DataGeneral implements IovGroupsCustom {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IovGroupsImpl.class);

    /**
     * The decoder.
     */
    private CharsetDecoder decoder = StandardCharsets.US_ASCII.newDecoder();

    /**
     * The upload directory for files.
     */
    @Value("${crest.upload.dir:/tmp}")
    private String serverUploadLocationFolder;

    /**
     * @param ds
     *            the DataSource
     */
    public IovGroupsImpl(DataSource ds) {
        super(ds);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.repositories.IovGroupsCustom#selectGroups(java.lang.String,
     * java.lang.Long)
     */
    @Override
    public List<BigDecimal> selectGroups(String tagname, Long groupsize) {
        log.info("Select Iov Groups for tag {} with group size {} using JDBCTEMPLATE", tagname,
                groupsize);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(getDs());
        final String tablename = getCrestTableNames().getIovTableName();
        // Set the default group frequency at 1000. This can be changed via groupsize argument.
        Long groupfreq = 1000L;
        if (groupsize != null && groupsize > 0) {
            groupfreq = groupsize;
        }
        final String sql = "select MIN(SINCE) from " + tablename + " where TAG_NAME=? "
                           + " group by floor(SINCE/?)*? "
                           + " order by min(SINCE)";
        log.debug("Execute selectGroups query {}", sql);
        return jdbcTemplate.queryForList(sql, BigDecimal.class, tagname, groupfreq, groupfreq);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.repositories.IovGroupsCustom#selectSnapshotGroups(java.lang.
     * String, java.util.Date, java.lang.Integer)
     */
    @Override
    public List<BigDecimal> selectSnapshotGroups(String tagname, Date snap, Long groupsize) {
        log.info(
                "Select Iov Snapshot Groups for tag {} with group size {} and snapshot time {} using JDBCTEMPLATE",
                tagname, groupsize, snap);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(getDs());
        final String tablename = getCrestTableNames().getIovTableName();
        // Set the default group frequency at 1000. This can be changed via groupsize argument.
        Long groupfreq = 1000L;
        if (groupsize != null && groupsize > 0) {
            groupfreq = groupsize;
        }
        final String sql = "select MIN(SINCE) from " + tablename
                           + " where TAG_NAME=? and INSERTION_TIME<=? "
                           + " group by floor(SINCE/?)*? "
                           + " order by min(SINCE)";
        log.debug("Execute selectSnapshotGroups query {}", sql);

        return jdbcTemplate.queryForList(sql, BigDecimal.class, tagname, snap, groupfreq,
                groupfreq);
    }

    /*
     * (non-Javadoc)
     *
     * @see hep.crest.data.repositories.IovGroupsCustom#getSize(java.lang.String)
     */
    @Override
    public Long getSize(String tagname) {
        log.info("Select count(TAG_NAME) Iov for tag {} using JDBCTEMPLATE", tagname);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(getDs());
        final String tablename = getCrestTableNames().getIovTableName();

        final String sql = "select COUNT(TAG_NAME) from " + tablename + " where TAG_NAME=?";
        log.info("Execute query {}", sql);
        return jdbcTemplate.queryForObject(sql, Long.class, tagname);
    }

    /*
     * (non-Javadoc)
     *
     * @see hep.crest.data.repositories.IovGroupsCustom#getSizeBySnapshot(java.lang.
     * String, java.util.Date)
     */
    @Override
    public Long getSizeBySnapshot(String tagname, Date snap) {
        log.info("Select count(TAG_NAME) Iov for tag {} and snapshot time {} using JDBCTEMPLATE",
                tagname, snap);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(getDs());
        final String tablename = getCrestTableNames().getIovTableName();

        final String sql = "select COUNT(TAG_NAME) from " + tablename
                           + " where TAG_NAME=? and INSERTION_TIME<=?";
        return jdbcTemplate.queryForObject(sql, Long.class, tagname, snap);
    }

    /*
     * (non-Javadoc)
     *
     * @see hep.phycdb.svc.repositories.IovGroupsCustom#getTagSummaryInfo(java.lang.
     * String)
     */
    @Override
    public List<TagSummaryDto> getTagSummaryInfo(String tagname) {
        log.info("Select count(TAG_NAME) Iov for tag matching pattern {} using JDBCTEMPLATE",
                tagname);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(getDs());
        final String tablename = getCrestTableNames().getIovTableName();
        // sql : count iovs in a tag
        // select TAG_NAME, COUNT(TAG_NAME) as NIOVS from IOV
        // where TAG_NAME like ? GROUP BY TAG_NAME
        final String sql = "select TAG_NAME, COUNT(TAG_NAME) as NIOVS from " + tablename
                           + " where TAG_NAME like ? GROUP BY TAG_NAME";
        return jdbcTemplate.query(sql, (rs, num) -> {
            final TagSummaryDto entity = new TagSummaryDto();
            entity.setTagname(rs.getString("TAG_NAME"));
            entity.setNiovs(rs.getLong("NIOVS"));
            return entity;
        }, tagname);
    }

}
