import crest from './gui/crest'
import iovForm from './gui/iovForm'

export default {
	namespaced: true,
	modules: {
		crest,
		iovForm
	},
}

