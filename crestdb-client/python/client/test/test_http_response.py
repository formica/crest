# coding: utf-8

"""
    CrestDB REST API

    Crest Rest Api to manage data for calibration files.  # noqa: E501

    OpenAPI spec version: 2.0
    Contact: andrea.formica@cern.ch
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import crestapi
from crestapi.models.http_response import HTTPResponse  # noqa: E501
from crestapi.rest import ApiException


class TestHTTPResponse(unittest.TestCase):
    """HTTPResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testHTTPResponse(self):
        """Test HTTPResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = crestapi.models.http_response.HTTPResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
