# IovSetDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**niovs** | **int** |  | [optional] 
**format** | **str** |  | [optional] 
**iovs_list** | [**list[IovDto]**](IovDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


