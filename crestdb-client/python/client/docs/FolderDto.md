# FolderDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node_fullpath** | **str** |  | [optional] 
**schema_name** | **str** |  | [optional] 
**node_name** | **str** |  | [optional] 
**node_description** | **str** |  | [optional] 
**tag_pattern** | **str** |  | [optional] 
**group_role** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


